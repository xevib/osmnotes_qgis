import requests
from lxml import etree
from io import BytesIO
from requests.auth import HTTPBasicAuth

class Comment(object):

    def __init__(self,comment):
        super().__init__()
        self.date = None
        self.uid = None
        self.user = None
        self.user_url = None
        self.text = None
        self.action = None
        self.html = None

        for attribute in comment:
            if attribute.tag == "date":
                self.date = attribute.text
            elif attribute.tag == "uid":
                self.uid = attribute.text
            elif attribute.tag == "user":
                self.user = attribute.text
            elif attribute.tag == "user_url":
                self.user_url = attribute.text
            elif attribute.tag == "text":
                self.text = attribute.text
            elif attribute.tag == "action":
                self.action = attribute.text
            elif attribute.tag == "html":
                self.html = attribute.text
            else:
                print(etree.tostring(attribute))


class Note(object):

    def __init__(self,element):
        super().__init__()
        self._lat = float(element.get("lat"))
        self._lon = float(element.get("lon"))
        self._comments = []
        self._ident = None
        self._url = None
        self._status = None
        self._date_created = None
        self._repoen_url = None
        self._date_closed = None
        self._comment_url = None
        for attribute  in element:
            if attribute.tag == "id":
                self._ident = attribute.text
            elif attribute.tag == "url":
                self._url = attribute.text
            elif attribute.tag == "status":
                self._status = attribute.text
            elif attribute.tag == "date_created":
                self._date_created = attribute.text
            elif attribute.tag == "reopen_url":
                self._repoen_url = attribute.text
            elif attribute.tag == "date_closed":
                self._date_closed = attribute.text
            elif attribute.tag == "comment_url":
                self._comment_url = attribute.text
            elif attribute.tag == "close_url":
                self._close_url = attribute.text
            elif attribute.tag == "comments":
                for comment in attribute.getchildren():
                    self._comments.append(Comment(comment))
            else:
                print(etree.tostring(attribute))

    @property
    def lat(self):
        return self._lat

    @lat.setter
    def set_lat(self, lat):
        self._lat = lat

    @property
    def lon(self):
        return self._lon

    @lon.setter
    def set_lon(self, lon):
        self._lon = lon

    @property
    def comments(self):
        return self._comments

    @comments.setter
    def set_comments(self,comments):
        self._comments = comments

    @property
    def ident(self):
        return self._ident

    @ident.setter
    def set_ident(self,ident):
        self._ident = ident

    @property
    def url(self):
        return self._url

    @url.setter
    def set_url(self,url):
        self._url = url

    @property
    def status(self):
        return self._status

    @status.setter
    def set_status(self,status):
        self._status = status

    @property
    def reopen_url(self):
        return self._repoen_url

    @reopen_url.setter
    def set_reopen_url(self,reopen_url):
        self._repoen_url = reopen_url

    @property
    def date_created(self):
        return self._date_created

    @date_created.setter
    def set_date_created(self,date_created):
        self._date_created = date_created

    @property
    def comment_url(self):
        return self._comment_url

    @comment_url.setter
    def set_comment_url(self,comment_url):
        self._comment_url = comment_url

    @property
    def date_closed(self):
        return self._date_closed

    @date_closed.setter
    def set_date_closed(self,date_closed):
        self._date_closed = date_closed

def close_note(ident,user,password,comment=None):
    params  ={}
    if comment:
        params["text"] = comment
    auth = HTTPBasicAuth(user, password)
    response = requests.post(f"https://api.openstreetmap.org/api/0.6/notes/{ident}/close/", params=params, auth=auth)
    return response.content

def get_notes(left,bottom,right,top):
    bbox = [str(left),str(bottom),str(right),str(top)]
    response = requests.get("https://api.openstreetmap.org/api/0.6/notes", params= {"bbox":",".join(bbox),"limit":10000})
    txt = BytesIO( bytes(response.text,"utf-8"))
    print(bbox)
    print(response.content)
    tree = etree.parse(txt)
    ret = []
    for element in tree.getroot():
        ret.append(Note(element))
    return ret


